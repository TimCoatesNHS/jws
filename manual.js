import fs from "fs";
import jose from 'node-jose';
const {
	createSign,
	createPrivateKey,
	getHashes
} = await import("crypto");
const { createHash } = await import("crypto");


/**
* Implements manual creation of a signed JWS following this process:
1) Include in the header:
x5c: public certdata
algo: RS256

2) Base64URL encode the header -> [B64 Header]

3) Include in the payload:
iat: now()
exp: now() + [validity in seconds]
plus: Whatever data you want to carry

4) Base64 encode the payload -> [B64 Payload]

5) Concatenate [B64 Header] +"." + [B64 Payload] = [Sign Input]

6) Sign [Sign Input] with the RSA-SHA256 algorithm = [Signature]

7) Base64URL encode [Signature] = [B64 Signature]

8) Signed JWS = [B64 Header] + "." + [B64 Payload] + "." + [B64 Signature]

* 
*/


// Load in the private key
let privatePEM = fs.readFileSync('./ca/fromsub2.key', 'utf8');
var privateKey = createPrivateKey({
	'key': privatePEM,
	'format': 'pem',
	'type': 'pkcs1',
});

// Load in the certificate
let certFile = fs.readFileSync('./ca/fromsub2.crt', 'utf8');
let certLines = certFile.split("\n");


let certData = "";
for (let line of certLines) {
	if (line.startsWith("-----") == false) {
		certData = certData + line;
	}
}

let header = {
	"alg": "RS256",
	"x5c": [certData]
};

// Some arbitrary data to carry
let body = {
	"hash": "f71a60f4847bc4ba555c4a2764bfeb26916c08ba6d254afc6cdde728f7dbac40",
	"exp": new Date().getTime() + (300 * 60),
	"iat": new Date().getTime()
};

let stringheader = JSON.stringify(header);
console.log("\nStringified header:\n" + stringheader);
let B64Header = jose.util.base64url.encode(stringheader);
console.log("\nBase64URL encoded header:\n" + B64Header);

let stringbody = JSON.stringify(body);
console.log("\nStringified body:\n" + stringbody);
let B64Body = jose.util.base64url.encode(stringbody);
console.log("\nBase64URL encoded body:\n" + B64Body);

let toSign = B64Header + "." + B64Body;
console.log("\nTo Sign: " + toSign);


let signature = signData(toSign, privateKey);


// NB: The signature of this JWS validates at:
console.log("\n\nComplete valid JWS:\n\n" + B64Header + "." + B64Body + "." + signature);

// These three lines allow printing out the JWS if we hash the data before signing it.
//let hashOfToSign = createHash('sha256').update(toSign).digest('hex');
//let signature2 = signData(hashOfToSign, privateKey);
//console.log("\n\nComplete MAYBE valid JWS:\n\n" + B64Header + "." + B64Body + "." + signature2);

console.log("\nTry it here:\nhttps://jwt.io/#debugger-io?token=" + B64Header + "." + B64Body + "." + signature);


function signData(data, privateKey) {
	//console.log(getHashes()); Gets the available options for createSign below
	const sign = createSign('RSA-SHA256');	// Here we get ready to sign some data
	sign.update(data);					// Pass in the data to be signed
	sign.end();								// And wrap it up
	let signature = sign.sign(privateKey);	// Get the resulting signature
	signature = jose.util.base64url.encode(signature);
	console.log("\Base64URL encoded SHA-256 Signature: " + signature);
	return signature;
}