#!/usr/bin/env node
import fs from "fs";
import jose from "node-jose";
import pem from "pem";
const { createHash } = await import("crypto");
import canonicalize from "canonicalize";


let filename = "./jws.dat";
console.log("Verifying JWS in file: " + filename);
let jws = fs.readFileSync(filename, "utf8");

let outcome = await validate(jws);



// Returns the signed payload or null
export async function validate(jwsString) {
	try {
		let verifyOptions = {
			allowEmbeddedKey: true, // We'll verify with a cert found in the JWS
			algorithms: ["RS1", "RS256", "RS384", "RS512"]	// Specify which algorithms we'll accept
		};

		let result = "";
		try {
			result = await jose.JWS.createVerify().verify(jwsString, verifyOptions);
			console.log("JWS Signature verified: ", true);

			// But .. it doesn't check expiry date on the message
			let exp = new Date(JSON.parse(result.payload).exp * 1000);

			if (Date.now() > exp) {
				console.log("\nSignature expired?: ", true);
				throw Error(`Signature is too old; JWS expires at: ${exp}`);
			} else {
				console.log("\nSignature expired?: ", false);
			}

			let iss = JSON.parse(result.payload).iss;
			let sub = JSON.parse(result.payload).sub;
			console.log("\nCertificate used to sign, was issued by: ", iss);
			console.log("\nCertificate used to sign, was issued to: ", sub);


			// and .. it doesn't do the full x509 cert verification, it just checks that the
			// key from the first cert in the x5c header can verify the payload so now, we
			// need to shell out to openssl to verify that the provided key was signed by the CA
			// why oh why is there nothing native for this

			// Here we recreate the signer's certificate from the x5c in the JWS header
			let recreatedCertificate = await x5c_to_cert(result.header.x5c);
			let payload = JSON.parse(result.payload);
			if (payload.hash == "2b5a72dfcaa2ca4c1dd71bc86f6c12f06fbc1d07ecf3ea5250c3a4740689964e") {
				console.log("\nPayload hash matches");
			} else {
				console.log("\nPayload hash doesn't match");
			}

			console.log("\nCertificate found in JWS header: " + recreatedCertificate);

			// Load the root CA here assumes that this is correct
			// Assume the recipient also has been given root / Sub CA Certs?
			let CACertificates = [
				fs.readFileSync("./DevCerts/DEVrootCA.pem", "utf8"),
				fs.readFileSync("./DevCerts/DEVSubCA.pem", "utf8")
			];

			// Check that the trust chain is okay!
			let trusted = await verifySigningChain(recreatedCertificate, CACertificates);
			if (trusted == false) {
				throw Error("\nCertificate signing this can't be traced");
			}

			console.log("\nSigning chain verified?: ", trusted);
			let certData = await getCertDetails(recreatedCertificate);
			console.log("Supplied certificate belongs to: " + certData.commonName);
			console.log("Supplied certificate was issued by: " + certData.issuer.organization);
			console.log("Signature Algorithm: " + certData.signatureAlgorithm);

			//let createDate = Date.parse(JSON.parse(JSON.parse(result.payload.toString()).data).Timestamp);
			let createDate = new Date(JSON.parse(result.payload.toString()).iat * 1000);
			if (createDate >= certData.validity.start && createDate <= certData.validity.end) {
				console.log("\nWas certificate valid when prescription signed?: ", true);
			} else {
				console.log("\nCert valid when prescription signed?: ", false);
				throw Error("Certificate wasn't valid at the time");
			}

			// Finally we're happy to show what was conveyed in the payload...
			//console.log(JSON.parse(result.payload));
			return result.payload.toString();
		} catch (Except) {
			console.log("\nJWS Verify (failing because `iat` isn't being added to the payload): " + Except);
		}


	} catch (err) {
		console.log(err);
		return null;
	}
}


// This does the equivalent of https://kb.wisc.edu/iam/page.php?id=4543
// i.e.: openssl verify -verbose -CAfile cacert.pem  server.crt
function verifySigningChain(cert, cacert) {
	return new Promise((resolve, reject) => {
		pem.verifySigningChain(cert, cacert, (err, ver) => {
			if (err) {
				return reject(err);
			}
			return resolve(ver);
		})
	});
}

// Build a Cert from the x5c x509 signing chain
function x5c_to_cert(x5c) {
	var cert, y;
	cert = ((function () {
		var _i, _ref, _results;
		_results = [];
		for (y = _i = 0, _ref = x5c.length; _i <= _ref; y = _i += 64) {
			_results.push(x5c.slice(y, +(y + 63) + 1 || 9e9));
		}
		return _results;
	})()).join("\n");
	return ("-----BEGIN CERTIFICATE-----\n" + cert + "\n-----END CERTIFICATE-----");
}

// Function to get the 
function getCertDetails(cert) {
	return new Promise((resolve, reject) => {
		pem.readCertificateInfo(cert, (err, data) => {
			if (err) {
				return reject(err);
			}
			return resolve(data);
		});
	});
}