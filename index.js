#!/usr/bin/env node
import fs from "fs";
import jose from "node-jose";
import pem from "pem";
const { createHash } = await import("crypto");
import canonicalize from "canonicalize";


let input = {
	// The message body, data we want to protect.
	"PrescriptionID": "48B07891-1D81-7B99-3BD2-23E242D2DD44",
	"Timestamp": "2022-02-07T10:30:00Z",
	"PatientName": "MRFRANKSIMONSMITH",
	"PatientNHSNumber": "9900002717",
	"PatientPostcode": "AA11AA",
	"PatientDoB": "1972-12-07",
	"PrescriberName": "DRSMITH",
	"PrescriberCode": "896547302565",
	"OrganisationODSCode": "M85011",
	"OrganisationPostcode": "AA22AA",
	"LineItemDetails": [
		{
			"LineItemID": "1E34EE53-397A-2E70-E97C-67B1BD95014D",
			"TerminologyCode": "243132000",
			"TerminologyName": "inhaled drug administration (procedure)",
			"DoseQuantity": "200",
			"RateQuantity": "4034511000001102",
			"ManufacturedProduct": "Salbutamol 100micrograms/actuation inhaler",
			"PertinentDosageInstructions": "2 Puffs when required"
		}, {
			"LineItemID": "791C9A65-5C6A-308B-D700-32D247BADB87",
			"TerminologyCode": "243132000",
			"TerminologyName": "inhaled drug administration (procedure)",
			"DoseQuantity": "240",
			"RateQuantity": "4034511000001102",
			"ManufacturedProduct": "Seretide 125 Evohaler (GlaxoSmithKline)",
			"PertinentDosageInstructions": "2 Puffs twice daily"
		}
	]
};

// First we canonicalise the JSON according to https://datatracker.ietf.org/doc/html/rfc8785
let canon = canonicalize(input);

// Now we Base64URL the data. Probably not required?
let b64data = Buffer.from(JSON.stringify(canon)).toString("base64");

// And now we SHA-256 hash it...
let hashed = createHash('sha256').update(b64data).digest('hex');
let hashed2 = createHash('sha1').update(b64data).digest('hex');
console.log("Initial SHA-256 hash: " + hashed);
console.log("Initial SHA-1 hash: " + hashed2);

// But should we just canonicalise it (to eg get the attribute order and whitespacing consistent) and send it 'as-is'?
let payload = {
	"hash": hashed,
	"exp": Date.now() + 2 * 60 * 1000, // expires in 2 minutes
	"iat": Date.now()
};


// As a string
let message = JSON.stringify(payload);



let jws = await signData(message, true, true);

//////
// This replaces a character in the 
// jws = jws.substring(0, 1850) + "A" + jws.substring(1851, 3500);

let outcome = await validate(jws, true);

//console.log(outcome);

let rehydrated = JSON.parse(outcome);
let retrievedHash = rehydrated.hash;
console.log("Hash in JWS was: " + retrievedHash);

/**
 * Function to convert a payload to a Signed JWS, it takes three parameters;
 * message - an arbitrary string that's going to be carried in the JWS, could be data or could be a hash.
 * correctKey - boolean used to cause a failure by signing with 'the wrong' key.
 * correctCert - boolean used to cause a failure by passing 'the wrong' certificate across.
 */
export async function signData(message, correctKey, correctCert) {
	try {

		// Load in the private key we'll be signing with.
		let privatePEM = fs.readFileSync("./ca/fromsub2.key", "utf8");

		//////
		// This replaces the signing key with a bad one, and this causes a failure
		if (correctKey == false) {
			privatePEM = fs.readFileSync("./badcerts/badLad.key", "utf8");
		}

		let privateKey = await jose.JWK.asKey(privatePEM, "pem");


		// And the public key (actually Certificate) we'll transport to the recipient in the JWS.
		let publicPEM = fs.readFileSync("./ca/fromsub2.crt", "utf8");

		//////
		// This replaces the Cert we pass with an incorrect one, and this causes a failure
		if (correctCert == false) {
			publicPEM = fs.readFileSync("./badcerts/badLad.crt", "utf8");
		}

		// We need the public key chain in x5c header. x5c header chain will be used during decode, a full cert can be provided to ensure
		// validation all the way to root https://tools.ietf.org/html/draft-ietf-jose-json-web-key-41#page-9
		// Unfortunately we can't use jwk, since jwk is only the *key* and not the full *certificate*, so ... x5c it is
		// x5c is the x509 Certificate Chain as described at: https://datatracker.ietf.org/doc/html/rfc7517#section-4.7
		let x5cChain = cert_to_x5c(publicPEM);

		// And signing options
		let signOptions = {
			format: "compact",
			fields: {
				x5c: x5cChain
			}
		};

		// Sign the "message" with the "privatekey", and include the "x5c" chain in the header fields
		let signedJWS = await jose.JWS.createSign(signOptions, privateKey).update(message, "utf8").final();

		// It's big
		console.log("JWS:\n" + signedJWS + "\n");

		return signedJWS;


	} catch (err) {
		console.log(err);
	}
}

// Returns the signed payload or null
export async function validate(jwsString, correctRootCA) {
	try {

		let verifyOptions = {
			allowEmbeddedKey: true, // We'll verify with a cert found in the JWS
			algorithms: ["RS256"]	// Specify which algorithms we'll accept
		};

		let result = await jose.JWS.createVerify().verify(jwsString, verifyOptions);

		// But .. it doesn't check expiry date on the message
		let exp = new Date(JSON.parse(result.payload).exp);
		//console.log(exp);
		if (Date.now() > exp) {
			console.log("Signature expired?: ", true);
			throw Error(`Signature is too old; JWS expires at: ${exp}`);
		} else {
			console.log("Signature expired?: ", false);
		}

		// and .. it doesn't do the full x509 cert verification, it just checks that the
		// key from the first cert in the x5c header can verify the payload so now, we
		// need to shell out to openssl to verify that the provided key was signed by the CA
		// why oh why is there nothing native for this

		// Here we recreate the signer's certificate from the x5c in the JWS header
		let recreatedCertificate = await x5c_to_cert(result.header.x5c);

		// Load the sub and root CAs here assumes that this is correct
		// Assume the recipient also has been given root / Sub CA Certs?
		let CACertificates = [
			fs.readFileSync("./ca/sub2.crt", "utf8"),
			fs.readFileSync("./ca/sub1.crt", "utf8"),
			fs.readFileSync("./ca/myCA.pem", "utf8"),
		];

		//////
		// This replaces the Root Cert we test against with an incorrect one, and this causes a failure
		if (correctRootCA == false) {
			CACertificates = [fs.readFileSync("./badcerts/wrongCA.pem", "utf8")];
		}

		// Check that the trust chain is okay!
		let trusted = await verifySigningChain(recreatedCertificate, CACertificates);
		if (trusted == false) {
			throw Error("Certificate signing this can't be traced");
		}

		console.log("Signing chain verified?: ", trusted);
		let certData = await getCertDetails(recreatedCertificate);
		console.log("Message was signed by: " + certData.commonName);
		console.log("Cert was issued by: " + certData.issuer.organization);
		console.log("Signature Algorithm: " + certData.signatureAlgorithm);

		//let createDate = Date.parse(JSON.parse(JSON.parse(result.payload.toString()).data).Timestamp);
		let createDate = new Date(JSON.parse(result.payload.toString()).iat);
		if (createDate >= certData.validity.start && createDate <= certData.validity.end) {
			console.log("Cert valid when prescription signed?: ", true);
		} else {
			console.log("Cert valid when prescription signed?: ", false);
			throw Error("Certificate wasn't valid at the time");
		}

		// Finally we're happy to show what was conveyed in the payload...
		//console.log(JSON.parse(result.payload));
		return result.payload.toString();

	} catch (err) {
		console.log(err);
		return null;
	}
}


// This does the equivalent of https://kb.wisc.edu/iam/page.php?id=4543
// i.e.: openssl verify -verbose -CAfile cacert.pem  server.crt
function verifySigningChain(cert, CACertArray) {
	return new Promise((resolve, reject) => {
		pem.verifySigningChain(cert, CACertArray, (err, ver) => {
			if (err) {
				return reject(err);
			}
			return resolve(ver);
		})
	});
}


// taken from (MIT licensed):
// https://github.com/hildjj/node-posh/blob/master/lib/index.js
function cert_to_x5c(cert, maxdepth) {
	if (maxdepth == null) {
		maxdepth = 0;
	}
	/*
	 * Convert a PEM-encoded certificate to the version used in the x5c element
	 * of a [JSON Web Key](http://tools.ietf.org/html/draft-ietf-jose-json-web-key).
	 *             
	 * `cert` PEM-encoded certificate chain
	 * `maxdepth` The maximum number of certificates to use from the chain.
	 */

	cert = cert.replace(/-----[^\n]+\n?/gm, ",").replace(/\n/g, "");
	cert = cert.split(",").filter(function (c) {
		return c.length > 0;
	});
	if (maxdepth > 0) {
		cert = cert.splice(0, maxdepth);
	}
	return cert;
}

// Build a Cert from the x5c x509 signing chain
function x5c_to_cert(x5c) {
	var cert, y;
	cert = ((function () {
		var _i, _ref, _results;
		_results = [];
		for (y = _i = 0, _ref = x5c.length; _i <= _ref; y = _i += 64) {
			_results.push(x5c.slice(y, +(y + 63) + 1 || 9e9));
		}
		return _results;
	})()).join("\n");
	return ("-----BEGIN CERTIFICATE-----\n" + cert + "\n-----END CERTIFICATE-----");
}

// Function to get the 
function getCertDetails(cert) {
	return new Promise((resolve, reject) => {
		pem.readCertificateInfo(cert, (err, data) => {
			if (err) {
				return reject(err);
			}
			return resolve(data);
		});
	});
}