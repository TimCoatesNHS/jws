let MR = require("./MedicationRequest.json");

let output = {};
output.id = MR.id;

for (contained of MR.contained) {

	if (contained.resourceType == "Patient") {
		for (identifier of contained.identifier) {
			if (identifier.system == "https://fhir.nhs.uk/Id/nhs-number") {
				output.NHSNumber = identifier.value;
			}
		}

		for (personName of contained.name) {
			if (personName.use == "official") {
				output.patientName = personName.prefix[0] + " " + personName.given[0] + " " + personName.family;
			}
		}


		output.patientDoB = contained.birthDate;

		console.log(contained.address[0]);
		if (contained.address[0].use == "home") {
			output.patientPostcode = contained.address[0].postalCode;
		}

	}
}
console.log(JSON.stringify(output, null, 2));